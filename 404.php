<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package _tk
 */

get_header(); ?>

<div id="page-hero">
					<div class="pageHero1" style="background-image:url(/wp-content/uploads/2017/01/marina_from_lighthouse_1390x537_acf_cropped-4.jpg)">
						<h1>404</h1>
					</div>	
				</div>
<div id="post-wrp" class="txt-center pg404">
	<?php // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>
	<section class="content-padder rror-404 not-found">

		<header>
			<h2 class="page-title"><?php _e( 'Oops! Something went wrong here.', '_tk' ); ?></h2>
		</header><!-- .page-header -->

		<div class="page-content">

			<p><?php _e( 'Nothing could be found at this location. Maybe try a search?', '_tk' ); ?></p>

			<form role="search" method="get" class="searchform group" action="<?php echo home_url( '/' ); ?>">
			 <label>
			 <span class="offscreen"><?php echo _x( 'Search for:', 'label' ) ?></span>
			 <input type="search" class="search-field"
			 placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
			 value="<?php echo get_search_query() ?>" name="s"
			 title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			 </label>
			 <!-- <input type="image" alt="Submit search query"
			 src="">-->
			</form>

		</div><!-- .page-content -->

	</section><!-- .content-padder -->
</div>
<?php get_footer(); ?>