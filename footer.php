<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<footer id="colophon" class="site-footer" role="contentinfo">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container-fluid">
		<div class="row">
			<div class="site-footer-inner col-sm-12">

				<div class="site-info col-xs-12 col-sm-4">
					<?php the_field('footer_copyright', 'option'); ?>
				</div><!-- close .site-info -->
				<div class="contact-info col-xs-12 col-sm-4">
					<img src="/wp-content/themes/KABA-theme/images/footer-logo.png"/>
					<div class="footer-links"><a class="yellow-btn" href="/about/contact">Contact Us</a> <a class="yellow-btn"  href="#" type="button" data-toggle="modal" data-target="#myModal">Join Newsletter</a></div>

					<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="myModalLabel">Join Newsletter</h4>
					      </div>
					      <div class="modal-body">
					      	<?php echo do_shortcode('[contact-form-7 id="800" title="Newsletter Signup"]'); ?>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        
					      </div>
					    </div>
					  </div>
					</div>

				</div><!-- close .site-info -->
				<div class="site-social col-xs-12 col-sm-4">
					
					<?php if( have_rows('social_media', 'option') ):   while ( have_rows('social_media', 'option') ) : the_row(); ?>                 
		            	<a href="<?php the_sub_field('social_media_url', 'option'); ?>" style="background-image:url(<?php the_sub_field('icon_image', 'option');?>)" class="social-btn" target="_blank"><?php the_sub_field('social_media_network', 'option');?></a>
		            <?php endwhile; endif;?>

				</div><!-- close .site-info -->

			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->

<?php wp_footer(); ?>
<script>
jQuery(function($) {



    function contactResizer(){
    	$('.contact-bx').each(function(){
    		var bxWidth = $(this).outerWidth();
    		if (bxWidth > 660 ){
    			$(this).find('.circular--landscape').css("float","left");
    			console.log(bxWidth, " greater then ");
    		}else {
    			$(this).find('.circular--landscape').removeAttr( 'style' );
    			console.log(bxWidth, "less then ");
    		}
    	})
    }
    contactResizer();
    $(window).resize(function(){
    	contactResizer();
    })



$('.navbar .dropdown').hover(function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

}, function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

});

$('.navbar .dropdown > a').click(function(){
location.href = this.href;
});

});
</script>
</body>
</html>
