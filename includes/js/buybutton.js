(function() {
    // Register buttons
    tinymce.create('tinymce.plugins.MyButtons', {
        init: function( editor, url ) {
            // Add button that inserts shortcode into the current position of the editor
            editor.addButton( 'my_button', {
                title: 'Blue Button',
                icon: false,
                onclick: function() {
                    // Open a TinyMCE modal
                    editor.windowManager.open({
                        title: 'Blue Button',
                        body: [{
                            type: 'textbox',
                            name: 'label',
                            label: 'Label'
                        },{
                            type: 'textbox',
                            name: 'link',
                            label: 'Link URL'
                        },{
                            type: 'checkbox',
                            name: 'window',
                            label: 'Open In A New Tab/Window',
                            id: 'check-autoplay',
                            classes: 'what'
                        }],
                        onsubmit: function( e ) {
                            //editor.insertContent( '[buybutton link="' + e.data.link + '" label="' + e.data.label + '" price="' + e.data.price + '"]' );
                            // editor.insertContent( '[blue-button link="' + e.data.link + '" target="' + e.data.window + '" ]' + e.data.label + '[/blue-button]' );
                            if(e.data.window === true) {

                                editor.insertContent( '[blue-button link="' + e.data.link + '" window="yes" ]' + e.data.label + '[/blue-button]' );
                            }else{
                               editor.insertContent( '[blue-button link="' + e.data.link + '" ]' + e.data.label + '[/blue-button]' );
                            }

                        }
                    });
                }
            });
            editor.addButton( 'contact_bx', {
                title: 'Contact Box',
                icon: false,
                onclick: function() {
                    // Open a TinyMCE modal
                    editor.windowManager.open({
                        title: 'Contact Box',
                        body: [{
                            type: 'textbox',
                            name: 'title',
                            label: 'Title'
                        },{
                            type: 'textbox',
                            name: 'ImageURL',
                            label: 'Image URL'
                        },{
                            type: 'textbox',
                            name: 'text',
                            label: 'Text'
                        },{
                            type: 'textbox',
                            name: 'email',
                            label: 'Email Address'
                        },{
                            type: 'textbox',
                            name: 'phone',
                            label: 'Phone'
                        }],
                         width: 600,
                         height: 400,
                        onsubmit: function( e ) {
                            //editor.insertContent( '[buybutton link="' + e.data.link + '" label="' + e.data.label + '" price="' + e.data.price + '"]' );
                            editor.insertContent( '<fieldset class="contact-bx"><legend>' + e.data.title +'</legend><div class="circular--landscape"><img src="' + e.data.ImageURL +'" /></div><p class="txt-wrp">' + e.data.text +'</p><p class="contact"><a href="mailto:' + e.data.email +'">' + e.data.email +'</a> | ' + e.data.phone +'</p></fieldset>' );
                        }
                    });
                }
            });
        },
        createControl: function( n, cm ) {
            return null;
        }
    });
    // Add buttons
    tinymce.PluginManager.add( 'my_button_script', tinymce.plugins.MyButtons );
})();
