<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package _tk
 */

get_header(); ?>

	<?php if ( have_posts() ) : ?>
<div id="post-wrp">
		<header>
			<h2 class="page-title"><?php printf( __( 'Search Results for: %s', '_tk' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		</header><!-- .page-header -->

		<?php // start the loop. ?>
		
		<?php while ( have_posts() ) : the_post(); ?>

			<div id="post-<?php the_ID(); ?>" class="col-xs-12 sm-6 col-md-6">
			<div class="posts-wrp search">
			   	
			    <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
			     <div class="postmetadata">
			        <?php the_tags('Tags: ', ', ', '<br />'); ?>
			        Posted in <?php the_category(', ') ?>  
			    </div>
			    <div class="entry">
			        <?php the_excerpt(); ?>
			    </div>
			    <a class="more-link btn blue-btn" href="<?php the_permalink()?>">View<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
			</div>
		</div>

		<?php endwhile; ?>

		<?php _tk_pagination(); ?>

	<?php else : ?>

		<?php get_template_part( 'no-results', 'search' ); ?>

	<?php endif; // end of loop. ?>

</div>
<?php get_footer(); ?>