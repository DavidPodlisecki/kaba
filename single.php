<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _tk
 */

get_header(); ?>
	<div id="post-wrp">
	<?php while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" class="col-xs-12 sm-12 col-md-12">
			<div class="post-wrp">
			   <?php  echo get_the_post_thumbnail( $post_id);	?>
			   	
			    <h4><?php the_title(); ?></h4>
			     <div class="postmetadata">
			        <?php the_tags('Tags: ', ', ', '<br />'); ?>
			        Posted in <?php the_category(', ') ?>  
			    </div>
			    <div class="entry">
			        <?php the_content(); ?>
			    </div>
			</div>
		</div>

	<?php endwhile; // end of the loop. ?>
	<a href="javascript: history.go(-1)" class="btn blue-btn"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Go Back </a>

</div>
<?php get_footer(); ?>