<?php if( have_rows('page_sections') ):  while ( have_rows('page_sections') ) : the_row();?>
			<?php if( get_row_layout() == 'two_column' ): ?>
				<div class="two_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-sm-12 col-md-6">
						<?php the_sub_field('col1');?>
					</div>
					<div class="col-sm-12 col-md-6">
						<?php the_sub_field('col2');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'two_column_circle_img' ): ?>
				<div class="two_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-sm-12 col-md-6 p04">
						<div class="circle-img" style="background:url(<?php the_sub_field('circle');?>);"></div>
					</div>
					<div class="col-sm-12 col-md-6 p4">
						<?php the_sub_field('col2');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'two_column_divider' ): ?>
				<div class="two_col_devider_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-sm-12 col-md-6 divider p08">
						<?php the_sub_field('col1');?>
					</div>
					<div class="col-sm-12 col-md-6 p08">
						<?php the_sub_field('col2');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'one_column' ): ?>
				<div class="one_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-sm-12 col-md-12">
						<?php the_sub_field('col');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'one_column_100_background_image' ): ?>
				<div class="one_col_wrp full-background" style="background-image:url(<?php the_sub_field('bk_img');?>); background-position:<?php the_sub_field('background-position');?> ; <?php if( get_sub_field('set_margins') ): ?><?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?><?php endif; ?>">
					<div class="col-sm-12 col-md-9 col-md-10 aligncenter">
						<?php the_sub_field('col');?>
					</div>
				</div>
			<?php endif; ?>

			<?php if( get_row_layout() == 'three_col_center_text' ): ?>
				<div class="col-sm-12 col-md-10 aligncenter" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-sm-12 col-md-4">
						<?php the_sub_field('col1');?>
					</div>
					<div class="col-sm-12 col-md-4 callout txt-center">
						<?php the_sub_field('col_2');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php the_sub_field('col_3');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'alternating_column_row' ): ?>
				<div class="alt-row-wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<?php if ( have_rows( 'row' ) ) : ?>
						<?php $counter = 1 ; ?>
						<?php while ( have_rows( 'row' ) ) : the_row(); ?>
						<?php
						if ( 0 === $counter % 2 ) {
							$class = 'img_left';
						} else {
							$class = 'img_right';
						}
						?>

							<div class="<?php echo $class;?> alt-rows">
								<div class="col-sm-12 col-md-6 blue-bx">
									<div class="col-sm-12 col-md-9 col-lg-9 row-txt">
										<h6><?php the_sub_field( 'title' );?></h6>
										<?php the_sub_field( 'text' );?>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 alt-img">
									<img src="<?php the_sub_field( 'image' );?>">
								</div>
							</div>

						<?php $counter++ ; ?>
						<?php endwhile; ?>

					<?php endif; ?>

				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'one_column_80' ): ?>
				<div class="one_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-sm-12 col-md-10 aligncenter">
						<?php the_sub_field('col');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'one_column_60' ): ?>
				<div class="one_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-sm-12 col-md-7 aligncenter">
						<?php the_sub_field('col');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'one_column_center_50_wologo' ): ?>
				<div class="one_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 txt-center aligncenter">
						<?php the_sub_field('col');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'one_column_center_50' ): ?>
				<div class="one_col_wrp logo" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
					<div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 txt-center aligncenter">
						<?php the_sub_field('col');?>
					</div>
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'advantage_bx' ): ?>
			<div class="one_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
				<div class="home-advantages">
					<?php if( have_rows('advantage') ):   while ( have_rows('advantage') ) : the_row(); ?>                  
		            	<div class="col-xs-12 col-sm-6 col-md-4">
		            		<a href="<?php the_sub_field('link_url');?>" class="advantage-wrp" style="background-image:url(<?php the_sub_field('background_image');?>)">
			                	<div class="advantage-txt">
				                	<div class="icon"><img src="/wp-content/themes/KABA-theme/images/yellow-icon.png"/></div>
				                	<h3 class="title"><?php the_sub_field('title'); ?></h3>
				                    <div class="sub-title"><?php the_sub_field('subtitle'); ?></div> 
			                    </div>                 
		                    </a>
		                </div>
		             <?php endwhile; endif;?> 
				</div>
			</div>	
			<?php endif;?>
			<?php if( get_row_layout() == 'file_section' ): ?>
			<div class="one_col_wrp" <?php if( get_sub_field('set_margins') ): ?>style="<?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?>" <?php endif; ?>>
				<div class="col-xs-12 col-sm-9 col-md-8 col-lg-7 txt-center aligncenter">
					<?php if( have_rows('file') ):   while ( have_rows('file') ) : the_row(); ?>                  
		            	
						<?php 

							$file = get_sub_field('upload_file');

							if( $file ): 

								// vars
								$url = $file['url'];
								$title = $file['title'];
								$caption = $file['caption'];
								

								// icon
								$icon = $file['icon'];
								
								if( $file['type'] == 'image' ) {
									
									$icon =  $file['sizes']['thumbnail'];
									
								}
								?>


								<a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 download">

									<img src="<?php if(get_sub_field('file_preview')){ the_sub_field('file_preview'); }else { echo $icon; }?>" />
									
									<span><?php echo $title; ?></span>

								<?php if( $caption ): ?>

										<p class="wp-caption-text"><?php echo $caption; ?></p>

								<?php endif; ?>

								</a>


							<?php endif; ?>


		             <?php endwhile; endif;?> 
				</div>
			</div>	
			<?php endif;?>
				<?php if( get_row_layout() == 'regional_destinations' ): ?>
				<div class="rgnl_dest" style="background-image:url(<?php the_sub_field('background_image');?>); <?php if( get_sub_field('set_margins') ): ?><?php if( get_sub_field('margin_top') ): ?>margin-top:<?php the_sub_field('margin_top');?>%;<?php endif;?><?php  if( get_sub_field('margin_bottom') ): ?>margin-bottom:<?php the_sub_field('margin_bottom');?>%;<?php endif; ?><?php endif; ?>">
					<h3 class="title"><?php the_sub_field('location_title'); ?></h3>
					<div class="col-sm-12 col-md-10 col-lg-8 aligncenter">
						<div class="cnt"><?php the_sub_field('location_text'); ?></div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="col-xs-7 col-sm-6 col-md-7 col-lg-8 aligncenter">
									<div class="circle-img blue" style="background:url(<?php the_sub_field('location1_background');?>);">
										<div class="txt-wrp"><span><?php the_sub_field('distance_in_minutes1');?></span>mins</div>
									</div>
								</div>
								<div class="local-wrp"><?php the_sub_field('location1');?></div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="col-xs-7 col-sm-6 col-md-7 col-lg-8 aligncenter">
									<div class="circle-img blue" style="background:url(<?php the_sub_field('location2_background');?>);">
										<div class="txt-wrp"><span><?php the_sub_field('distance_in_minutes2');?></span>mins</div>
									</div>
								</div>
								<div class="local-wrp"><?php the_sub_field('location2');?></div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="col-xs-7 col-sm-6 col-md-7 col-lg-8 aligncenter">
									<div class="circle-img blue" style="background:url(<?php the_sub_field('location3_background');?>);">
										<div class="txt-wrp"><span><?php the_sub_field('distance_in_minutes3');?></span>mins</div>
									</div>
								</div>
								<div class="local-wrp"><?php the_sub_field('location3');?></div>
							</div>
						</div>
						<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="col-xs-7 col-sm-6 col-md-7 col-lg-8 aligncenter">
								<div class="circle-img yellow" style="background:url(<?php the_sub_field('location4_background');?>);">
									<div class="txt-wrp"><span><?php the_sub_field('distance_in_minutes4');?></span>mins</div>
								</div>
							</div>
							<div class="local-wrp"><?php the_sub_field('location4');?></div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="col-xs-7 col-sm-6 col-md-7 col-lg-8 aligncenter">
								<div class="circle-img yellow" style="background:url(<?php the_sub_field('location5_background');?>);">
									<div class="txt-wrp"><span><?php the_sub_field('distance_in_minutes5');?></span>mins</div>
								</div>
							</div>
							<div class="local-wrp"><?php the_sub_field('location5');?></div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="col-xs-7 col-sm-6 col-md-7 col-lg-8 aligncenter">
								<div class="circle-img yellow" style="background:url(<?php the_sub_field('location6_background');?>);">
									<div class="txt-wrp"><span><?php the_sub_field('distance_in_minutes6');?></span>mins</div>
								</div>
							</div>
							<div class="local-wrp"><?php the_sub_field('location6');?></div>
						</div>
					</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		<?php endwhile; endif;?> 