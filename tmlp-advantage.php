<?php
/**
 * Template Name: Advantage Parent
 * Description: Advantage Parent page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php 
// determine parent of current page
if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $parent = $ancestors[count($ancestors) - 1];
} else {
    $parent = $post->ID;
}

$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");

if ($children) {
?>

    <ul class="subnav">
        <?php 
            // current child will have class 'current_page_item'
            echo $children; 
        ?>
    </ul>

<?php 
} 
?>		
	<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>
