<?php
/**
 * Template Name: Child Company Case Study Page
 * Description: General Child page template with Child Company Case Studies
 *
 * @package _tk
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/child', 'hero' ); ?>

				<!-- get_template_part( 'post/content', get_post_format() ); -->
				
				<?php 
		// determine parent of current page
		if ($post->post_parent) {
		    $ancestors = get_post_ancestors($post->ID);
		    $parent = $ancestors[count($ancestors) - 1];
		} else {
		    $parent = $post->ID;
		}

		$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");

		if ($children) {
		?>

		   <div class="subnav-wrp">
				    <ul class="subnav">
				        <?php 
				            // current child will have class 'current_page_item'
				            echo $children; 
				        ?>
				    </ul>
				</div>
		<?php 
		} 
		?>		
	<?php endwhile; // end of the loop. ?>

	<div class="one_col_wrp">
		<div class="col-sm-12 col-md-10 col-lg-8 aligncenter">
			<?php the_field('intro_text');?>
		</div>
		<div id="cs-link-wrp" class="col-sm-12 col-md-10 col-lg-8 aligncenter">
			<?php if( have_rows('case_studies') ): ?>
				 <?php $i = 1; while ( have_rows('case_studies') ) : the_row();?>
				 <a class="btn blue-btn" href="#case_study_row<?php echo $i; ?>"><?php the_sub_field('company_name');?><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
				 <?php $i++; endwhile;?>
			<?php endif;?> 
		</div>
	</div>
	<?php if( have_rows('case_studies') ): ?>
		 <div class="col-sm-12 col-md-10 col-lg-9 aligncenter">
		 <?php $i = 1; while ( have_rows('case_studies') ) : the_row();?>
		 	<div class="row" id="case_study_row<?php echo $i; ?>">
		 		<div class="logo-divdr"></div>
				<?php if( get_row_layout() == 'case_study' ): ?>
					<div class="col-sm-12 col-md-7">
						<div class="cs_col1">
							<h2><?php the_sub_field('company_name');?></h2>
							<?php the_sub_field('column_one'); ?>
						</div> 
					</div>
					<div class="col-sm-12 col-md-5">
						<div class="cs_col2">
							<?php if( have_rows('column_two')):  while ( have_rows('column_two')): the_row();?>
								<div class="gray_side_row">
									<?php the_sub_field( 'row' );?>
								</div>
							<?php endwhile; endif; ?>
						</div>
					</div>	
				<?php endif;?>
			</div>
		<?php $i++; endwhile;?>
		</div>
	<?php endif;?> 
	<script>
		jQuery(function($) {
			$('a[href*="#"]:not([href="#"])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html, body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
	</script>

<?php get_footer(); ?>
