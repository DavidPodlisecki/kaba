<?php
/**
 * Template Name: Child Page
 * Description: General Child page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/child', 'hero' ); ?>

				<!-- get_template_part( 'post/content', get_post_format() ); -->
				
				<?php 
		// determine parent of current page
		if ($post->post_parent) {
		    $ancestors = get_post_ancestors($post->ID);
		    $parent = $ancestors[count($ancestors) - 1];
		} else {
		    $parent = $post->ID;
		}

		$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");

		
		if ($children) {
			 
		?>

		   <div class="subnav-wrp">
				    <ul class="subnav">
				        <?php 
				            // current child will have class 'current_page_item'
				            //echo $children; 
				        $output = wp_list_pages( array(
			                // Only pages that are children of the current page
			                'child_of' => $parent,
			                // Only show one level of hierarchy
			                'depth' => 1,
			                'title_li' => ''
			            ) );
				        ?>
				    </ul>
				</div>
		<?php 
		} 
		?>		
	<?php endwhile; // end of the loop. ?>

		<?php get_template_part( 'template-parts/page', 'modules' ); ?>
	
<?php get_footer(); ?>
