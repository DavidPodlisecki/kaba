<?php
/**
 * Template Name: Child Page | Staff
 * Description: Staff Page for Child page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/child', 'hero' ); ?>

				<!-- get_template_part( 'post/content', get_post_format() ); -->


				<?php
		// determine parent of current page
		if ($post->post_parent) {
		    $ancestors = get_post_ancestors($post->ID);
		    $parent = $ancestors[count($ancestors) - 1];
		} else {
		    $parent = $post->ID;
		}

		$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");


		if ($children) {

		?>

		   <div class="subnav-wrp">
				    <ul class="subnav">
				        <?php
				            // current child will have class 'current_page_item'
				            //echo $children;
				        $output = wp_list_pages( array(
			                // Only pages that are children of the current page
			                'child_of' => $parent,
			                // Only show one level of hierarchy
			                'depth' => 1,
			                'title_li' => ''
			            ) );
				        ?>
				    </ul>
				</div>
		<?php
		}
		?>
	<?php endwhile; // end of the loop. ?>
	<div class="cnt-wrp">
		<div class="white">
			<div class="col-sm-12 col-md-10 aligncenter pt5">
				<h2><?php the_field('section_one_title');?></h2>
				<?php the_field('section_one_subtitle_text');?>
			</div>
		</div>
		<?php if( have_rows('staff') ): ?>
		<div class="gray staff-wrapr">
			<?php while ( have_rows('staff')): the_row();?>
			<div class="staff-wrp  col-xs-12 col-sm-6 col-md-4">
				<div class="col-sm-12 aligncenter">
					<div class="col-sm-12 col-md-8 aligncenter p04">
						<div class="circle-img profilepic" style="background:url(<?php the_sub_field('profile_photo');?>);"></div>
					</div>
				</div>
				<div class="col-sm-12 aligncenter p4">
					<h4 class="name"><?php the_sub_field('name');?></h4>
					<span class="title"><?php the_sub_field('title');?></span>
					<div class="conact-wrp">
						<?php if(get_sub_field('email')):?>
							<a href="mailto:<?php the_sub_field('email');?>"><?php the_sub_field('email');?></a> |
						<?php endif?>
						<?php the_sub_field('phone');?>
					</div>
				</div>
			</div>
			<?php endwhile;?>
		</div>
		<?php endif;?>


		<div class="white">
			<div class="col-sm-12 col-md-10 aligncenter pt4">
				<h2><?php the_field('section_two_title');?></h2>
				<?php the_field('section_two_subtitle_text');?>
			</div>
		</div>
		<?php if( have_rows('board') ): ?>
		<div class=" white board staff-wrapr">
			<?php while ( have_rows('board')): the_row();?>
			<div class="staff-wrp  col-xs-6 col-sm-4 col-md-3">
				<div class="col-sm-12 aligncenter">
					<div class="col-sm-12 col-md-8 aligncenter p04">
						<div class="circle-img profilepic" style="background:url(<?php the_sub_field('profile_photo');?>);"></div>
					</div>
				</div>
				<div class="col-sm-12 aligncenter p4">
					<? if(get_sub_field('member_title')){?><span class="member-title"><?php the_sub_field('member_title');?></span><?php }?>
					<span class="title"><?php the_sub_field('name');?></span>

					<div class="conact-wrp">
						<?php the_sub_field('title');?>
					</div>
				</div>
			</div>
			<?php endwhile;?>
		</div>
		<?php endif;?>


	</div>
	<?php get_template_part( 'template-parts/page', 'modules' ); ?>
	<?php if( have_rows('page_modules') ):  while ( have_rows('page_modules') ) : the_row();?>
	<?php if( get_row_layout() == 'row_boxes' ): ?>
			<div class="one_col_wrp row-mod-wrp">
				<div class="home-advantages">
					<?php if( have_rows('box') ):   while ( have_rows('box') ) : the_row(); ?>
		            	<div class="col-xs-12 col-sm-6 col-md-4 row-mod">
		            		<a href="<?php the_sub_field('link_url');?>" class="advantage-wrp" style="background-image:url(<?php the_sub_field('background_image');?>)">
			                	<div class="advantage-txt">
				                	<div class="icon"><img src="/wp-content/themes/KABA-theme/images/yellow-icon.png"/></div>
				                	<h3 class="title"><?php the_sub_field('title'); ?></h3>
				                    <div class="sub-title"><?php the_sub_field('subtitle'); ?></div>
			                    </div>
		                    </a>
		                </div>
		             <?php endwhile; endif;?>
				</div>
			</div>
			<?php endif;?>
		<?php endwhile; endif;?>


<?php get_footer(); ?>
