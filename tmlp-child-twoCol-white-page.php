<?php
/**
 * Template Name: Child Page | Two Column White 
 * Description: Two Column Gray for Child page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/child', 'hero' ); ?>

				<!-- get_template_part( 'post/content', get_post_format() ); -->
				
				<?php 
		// determine parent of current page
		if ($post->post_parent) {
		    $ancestors = get_post_ancestors($post->ID);
		    $parent = $ancestors[count($ancestors) - 1];
		} else {
		    $parent = $post->ID;
		}

		$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");

		if ($children) {
		?>

		   <div class="subnav-wrp">
				    <ul class="subnav">
				        <?php 
				            // current child will have class 'current_page_item'
				            echo $children; 
				        ?>
				    </ul>
				</div>
		<?php 
		} 
		?>		
	<?php endwhile; // end of the loop. ?>
	<div class="cnt-wrp">
		<div class="white">
			<div class="col-sm-12 col-md-6 p7">
				<?php the_field( 'column_1' );?>
			</div>
			<div class="col-sm-12 col-md-6 p7">
				<?php if ( have_rows( 'column_two' ) ) : ?>
					
						<?php while ( have_rows( 'column_two' ) ) : the_row(); ?>
						<div class="gray_side_row">
							<?php the_sub_field( 'row' );?>
						</div>
						<?php endwhile; ?>

					<?php endif; ?>
				
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/page', 'modules' ); ?>
	<?php if( have_rows('page_modules') ):  while ( have_rows('page_modules') ) : the_row();?>
	<?php if( get_row_layout() == 'row_boxes' ): ?>
			<div class="one_col_wrp row-mod-wrp">
				<div class="home-advantages">
					<?php if( have_rows('box') ):   while ( have_rows('box') ) : the_row(); ?>                  
		            	<div class="col-xs-12 col-sm-6 col-md-4 row-mod">
		            		<a href="<?php the_sub_field('link_url');?>" class="advantage-wrp" style="background-image:url(<?php the_sub_field('background_image');?>)">
			                	<div class="advantage-txt">
				                	<div class="icon"><img src="/wp-content/themes/KABA-theme/images/yellow-icon.png"/></div>
				                	<h3 class="title"><?php the_sub_field('title'); ?></h3>
				                    <div class="sub-title"><?php the_sub_field('subtitle'); ?></div> 
			                    </div>                 
		                    </a>
		                </div>
		             <?php endwhile; endif;?> 
				</div>
			</div>	
			<?php endif;?>
		<?php endwhile; endif;?>


<?php get_footer(); ?>
