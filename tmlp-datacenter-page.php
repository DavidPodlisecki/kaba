<?php
/**
 * Template Name: Child Page | Data Center
 * Description: Data Center Page for Child page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/child', 'hero' ); ?>

				<!-- get_template_part( 'post/content', get_post_format() ); -->


				<?php
		// determine parent of current page
		if ($post->post_parent) {
		    $ancestors = get_post_ancestors($post->ID);
		    $parent = $ancestors[count($ancestors) - 1];
		} else {
		    $parent = $post->ID;
		}

		$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");


		if ($children) {

		?>

		   <div class="subnav-wrp">
				    <ul class="subnav">
				        <?php
				            // current child will have class 'current_page_item'
				            //echo $children;
				        $output = wp_list_pages( array(
			                // Only pages that are children of the current page
			                'child_of' => $parent,
			                // Only show one level of hierarchy
			                'depth' => 1,
			                'title_li' => ''
			            ) );
				        ?>
				    </ul>
				</div>
		<?php
		}
		?>
	<?php endwhile; // end of the loop. ?>
	<div class="cnt-wrp">
			<div class="tab-wrp" style="margin-top:5%;">
				<div id="exTab1" class="container">
				<?php if(have_rows('tabs')): $tabcounter = 0 ?>
					<ul  class="nav nav-tabs data-nav">
					<?php while (have_rows('tabs')): $tabcounter++; the_row();?>
							<li <?php if ($tabcounter == 1) {echo 'class="active"';} ?>>
								<a  href="#<?php echo $tabcounter; ?>" data-toggle="tab"><?php the_sub_field('tab_title'); ?></a>
							</li>
					<?php endwhile;?>
					</ul>
				<?php endif; ?>


				<?php if(have_rows('tabs')): $panecounter = 0 ?>
					<div class="tab-content clearfix">
					<?php while (have_rows('tabs')): $panecounter++; the_row();?>
							<div class="tab-pane data-pane <?php if ($panecounter == 1) {echo 'active';} ?>" id="<?php echo $panecounter; ?>">
								<?php if( get_sub_field('title') ): ?><h2><?php the_sub_field('title'); ?></h2><?php endif; ?>
								<?php if( get_sub_field('sub_title') ): ?><div class="tab-txt"><?php the_sub_field('sub_title'); ?></div><?php endif?>
								<?php if(have_rows('featured_files')):?>

									<?php while(have_rows('featured_files')): the_row();?>

										<?php
										$file = get_sub_field('file');
										if( $file ):

											// vars
											$url = $file['url'];
											$title = $file['title'];
											$caption = $file['caption'];


											// icon
											$icon = $file['icon'];

											if( $file['type'] == 'image' ) {

												$icon =  $file['sizes']['thumbnail'];

											}
											?>

											<a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 download m2-0">

												<img src="<?php if(get_sub_field('preview_image')){ the_sub_field('preview_image'); }else { echo $icon; }?>" />

												<span class="col-xs-12 col-sm-12"><?php echo $title; ?></span>



											</a>


										<?php endif; ?>

									<?php endwhile;?>
								<?php endif;?>
								<?php if(have_rows('list_files')):?>
									<div class="col-12 m3-0">
									<div class="col-xs-12 col-sm-8 col-md-7 aligncenter">
									<?php while(have_rows('list_files')): the_row();?>
										<?php
										$file = get_sub_field('file');
										if( $file ):

											// vars
											$url = $file['url'];
											$title = $file['title'];
											$caption = $file['caption'];



											?>

											<a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="col-xs-12 col-sm-6 col-md-6"><span><?php echo $title; ?></span></a>


										<?php endif; ?>

									<?php endwhile;?>
								</div>
							</div>
								<?php endif;?>

							</div>
					<?php endwhile;?>
					</div>
				<?php endif; ?>


			</div>


	</div>
	<!--
	<?php get_template_part( 'template-parts/page', 'modules' ); ?>
	<?php if( have_rows('page_modules') ):  while ( have_rows('page_modules') ) : the_row();?>
	<?php if( get_row_layout() == 'row_boxes' ): ?>
			<div class="one_col_wrp row-mod-wrp">
				<div class="home-advantages">
					<?php if( have_rows('box') ):   while ( have_rows('box') ) : the_row(); ?>
		            	<div class="col-xs-12 col-sm-6 col-md-4 row-mod">
		            		<a href="<?php the_sub_field('link_url');?>" class="advantage-wrp" style="background-image:url(<?php the_sub_field('background_image');?>)">
			                	<div class="advantage-txt">
				                	<div class="icon"><img src="/wp-content/themes/KABA-theme/images/yellow-icon.png"/></div>
				                	<h3 class="title"><?php the_sub_field('title'); ?></h3>
				                    <div class="sub-title"><?php the_sub_field('subtitle'); ?></div>
			                    </div>
		                    </a>
		                </div>
		             <?php endwhile; endif;?>
				</div>
			</div>
			<?php endif;?>
		<?php endwhile; endif;?>
	-->
<script type="text/javascript">
	jQuery(document).ready(function($){

	$('#exTab1').tabCollapse({
    tabsClass: 'hidden-sm hidden-xs',
    accordionClass: 'visible-sm visible-xs'
	});

	});
</script>
<?php get_footer(); ?>
