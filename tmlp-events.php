<?php
/**
 * Template Name: Events
 * Description: General Child page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/child', 'hero' ); ?>

				<!-- get_template_part( 'post/content', get_post_format() ); -->
				
				<?php 
		// determine parent of current page
		if ($post->post_parent) {
		    $ancestors = get_post_ancestors($post->ID);
		    $parent = $ancestors[count($ancestors) - 1];
		} else {
		    $parent = $post->ID;
		}

		$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");

		if ($children) {
		?>

		   <div class="subnav-wrp">
				    <ul class="subnav">
				        <?php 
				            // current child will have class 'current_page_item'
				            echo $children; 
				        ?>
				    </ul>
				</div>
		<?php 
		} 
		?>		
	<?php endwhile; // end of the loop. ?>
	<div id="posts-wrp">
		<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		query_posts('post_type=events&posts_per_page=9&paged=' . $paged); 
		?>

		<?php if (have_posts()) : while ( have_posts()) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" class="col-xs-12 sm-6 col-md-4">
			<div class="posts-wrp">
			   	<a href="<?php the_permalink() ?>"><?php  echo get_the_post_thumbnail( $post_id, 'alternating-col');	?></a>
			   	
			    <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
			     <div class="postmetadata">
			        <?php the_tags('Tags: ', ', ', '<br />'); ?>
			        Posted in <?php the_category(', ') ?>  
			    </div>
			    <div class="entry">
			        <?php the_excerpt(); ?>
			    </div>
			    <a class="hidden-xs hidden-sm visible-md visible-lg more-link btn blue-btn" href="<?php the_permalink()?>">Continue reading<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
			</div>
		</div>

		<?php endwhile; endif; ?>

		<div class="navigation">
		<div class="next-posts"><?php next_posts_link('&laquo; Older Entries') ?></div>
		<div class="prev-posts"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>
	</div>
<?php get_footer(); ?>
