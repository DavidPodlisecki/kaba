<?php
/**
 * Template Name: Home
 * Description: Home page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php if( have_rows('home_sections') ):  while ( have_rows('home_sections') ) : the_row();?>
		<?php if( get_row_layout() == 'home_hero' ): ?>
		<div id="home-hero">
			<div class="col-xs-4 col-sm-4 col-md-4 row-bx" style="background-image:url(<?php the_sub_field('column_1_image');?>)">
			   	<!-- <a href="<?php the_sub_field('column_1_link');?>" class="bx-link"><?php the_sub_field('column_1_link_text');?> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a> -->
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 row-bx" style="background-image:url(<?php the_sub_field('column_2_image');?>)">
			   	<!-- <a href="<?php the_sub_field('column_2_link');?>" class="bx-link"><?php the_sub_field('column_2_link_text');?> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a> -->
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 row-bx" style="background-image:url(<?php the_sub_field('column_3_image');?>)">
			   	<!-- <a href="<?php the_sub_field('column_3_link');?>" class="bx-link"><?php the_sub_field('column_3_link_text');?> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a> -->
			</div>
			<fieldset class="hero-txt home-hero">
				 <legend><img src="<?php bloginfo('template_directory'); ?>/images/Ornament-hero.png"/></legend>
				 Perfectly <strong>Centered</strong>
				 <div class="ln-wrp"><span>See everything Kenosha has to offer.</span></div>
			</fieldset>
		</div>
		<?php endif; ?>
		<?php if( get_row_layout() == 'home_about' ): ?>
		<div id="home-about-wrp">
			<div class="home-about">
				<div class="col-xs-12 col-md-6 pr8">
					<h3><?php the_sub_field('left_title'); ?></h3>
					<?php if( get_sub_field('link_text_1') ): ?>
						<a href="<?php the_sub_field('link_url_1');?>" class="about-link"><?php the_sub_field('link_text_1');?></a>
					<?php endif; ?>
					<?php if( get_sub_field('link_text_2') ): ?>
						<a href="<?php the_sub_field('link_url_2');?>" class="about-link"><?php the_sub_field('link_text_2');?></a>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-md-6">
					<?php the_sub_field('right_content');?>
				</div>
			</div>
		</div>
		<?php endif;?>
		<?php if( get_row_layout() == 'home_advantages' ): ?>
		<div class="home-advantages">
			<?php if( have_rows('advantage') ):   while ( have_rows('advantage') ) : the_row(); ?>                  
            	<div class="col-xs-12 col-sm-6 col-md-4">
            		<a href="<?php the_sub_field('link_url');?>" class="advantage-wrp" style="background-image:url(<?php the_sub_field('background_image');?>)">
	                	<div class="advantage-txt">
		                	<div class="icon"><img src="/wp-content/themes/KABA-theme/images/yellow-icon.png"/></div>
		                	<h3 class="title"><?php the_sub_field('title'); ?></h3>
		                    <div class="sub-title"><?php the_sub_field('subtitle'); ?></div> 
	                    </div>                 
                    </a>
                </div>
             <?php endwhile; endif;?> 
		</div>
		<?php endif;?>
		<?php if( get_row_layout() == 'home_location'):?>
		<div id="home-location">
			<div class="location-txt">
				<h3 class="blue-txt"><?php the_sub_field('title');?></h3>
				<?php the_sub_field('text');?>
				<?php if(get_sub_field('link_text') ):?>
				<a href="<?php the_sub_field('link_url');?>" class="btn blue-btn"><?php the_sub_field('link_text');?> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
				<?php endif;?>
			</div>
		</div>
		<?php endif; ?>
		<?php  endwhile; endif;	?>
		
	<?php endwhile; // end of the loop. ?>
	<div id="home-news">
		<div class="col-xs-12 col-md-4 mb6">
			<h3 class="blue-txt">Upcoming Events</h3>
			<?php $eventloop = new WP_Query( array( 'post_type' => 'events', 'posts_per_page' => 2 ) ); ?>
			<?php while ( $eventloop->have_posts() ) : $eventloop->the_post(); ?>
			<div class="home-post">
				<div class="post-title"><?php the_title();?> Date</div>
				<div class="sub-title">Location Goes Here</div>
				<p><?php the_excerpt();?></p>
				<a href="<?php the_permalink();?>">View Event</a>
			</div>
			<?php endwhile; wp_reset_query(); ?>
			
		</div>
		<div class="col-xs-12 col-md-4 mb6">
			<h3 class="blue-txt">Inspire Kenosha</h3>
			<?php $eventloop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 2 ) ); ?>
			<?php while ( $eventloop->have_posts() ) : $eventloop->the_post(); ?>
			<div class="home-post">
				<div class="post-title"><?php the_title();?></div>
				<div class="sub-title"><?php the_date('F j, Y'); ?></div>
				<p><?php the_excerpt();?></p>
				<a href="<?php the_permalink();?>">View Event</a>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<div class="col-xs-12 col-md-4 mb6">
			<?php if ( is_active_sidebar( 'twitter-widget' ) ) : ?>
				<?php dynamic_sidebar('twitter-widget'); ?>
			<?php endif ;?>
		</div>
	</div>
<?php get_footer(); ?>
