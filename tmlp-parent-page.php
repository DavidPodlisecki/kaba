<?php
/**
 * Template Name: Parent Page
 * Description: General Parent page template
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php if( have_rows('hero_section') ):  while ( have_rows('hero_section') ) : the_row();?>
			<?php if( get_row_layout() == 'full_image' ): ?>
				<div id="page-hero">
					<div class="pageHero1" style="background-image:url(<?php the_sub_field('background_image');?>)">
						<h1><?php the_sub_field('hero_title');?></h1>
					</div>	
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'split_image' ): ?>
				<div id="page-hero">
					<div class="pageHero2" style="background-image:url(<?php the_sub_field('background_image');?>)">
						<h1 class="split"><?php the_sub_field('hero_title');?></h1>
						<div class="hidden-xs hidden-xs visible-md visible-lg hero_text"><?php the_sub_field('hero_text');?></div>
					</div>	
				</div>
			<?php endif; ?>
			<?php if( get_row_layout() == 'three_image' ): ?>
				<div id="page-hero">
					<div class="col-sm-4 col-md-4 row-bx" style="background-image:url(<?php the_sub_field('background_image1');?>)">
					</div>
					<div class="col-sm-4 col-md-4 row-bx" style="background-image:url(<?php the_sub_field('background_image2');?>)">
					</div>
					<div class="col-sm-4 col-md-4 row-bx" style="background-image:url(<?php the_sub_field('background_image3');?>)">
					</div>
					<h1 class="3colHero"><?php the_sub_field('hero_title');?></h1>
				</div>
			<?php endif; ?>
		<?php endwhile; endif;?> 
		
		<?php 
		// determine parent of current page
		if ($post->post_parent) {
		    $ancestors = get_post_ancestors($post->ID);
		    $parent = $ancestors[count($ancestors) - 1];
		} else {
		    $parent = $post->ID;
		}

		$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");

		if ($children) {
		?>
		<div class="subnav-wrp">
		    <ul class="subnav">
		        <?php 
		            // current child will have class 'current_page_item'
		            echo $children; 
		        ?>
		    </ul>
		</div>
		<?php 
		} 
		?>	

		<?php get_template_part( 'template-parts/page', 'modules' ); ?>
	

	<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>
